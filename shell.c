#include <fcntl.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
int global=0;
/*function to print the prompt*/
void type_prompt(){
	char* username = getlogin();/*get the username*/
	char dirname[1024];
	getcwd(dirname, sizeof(dirname));/*get the current directory path*/
	if(global==0){/*clear terminal only first time*/
		global++;
		printf("\033[H\033[J");
		printf("Welcome to my shell\nFor exiting use the command \"exit\".\n");
	}
	printf("[cs345sh][%s][%s]~$ ",username,dirname);
	return;
}
/*--------------------------------------------------------------------------------------*/
/*function used to redirect input from stdin to a given file*/
void change_input(char* filename){
	int fp=open(filename, O_RDONLY);
	dup2(fp,STDIN_FILENO);
	close(fp);
	return;
}
/*-----------------------------------------------------------------------*/
/*same as above but with output this time*/
void change_output(char* filename){
        int fp=open(filename, O_WRONLY | O_CREAT, 0777);
        dup2(fp,STDOUT_FILENO);
	close(fp);
	return;
}
/*-----------------------------------------------------------------------*/
/*same but it appends file*/
void change_append(char* filename){
        int fp=open(filename,  O_WRONLY | O_CREAT | O_APPEND);
        dup2(fp,STDOUT_FILENO);
	close(fp);
	return;
}
/*-----------------------------------------------------------------------*/
/*manages which of the three redirection to use depending on a given flag*/
void check_red(int flag,char* filename){
        if(flag==1){
                change_input(filename);
        }else if(flag==2){
                change_output(filename);
        }else if(flag==3){
                change_append(filename);
        }
        return;
}
/*-------------------------------------------------------------------------------------*/
/*restores redirection to defaults*/
void uncheck_red(){
	int tmpin=dup(0);
    	int tmpout=dup(1);
	dup2(tmpin,0);
        dup2(tmpout,1);
        close(tmpin);
        close(tmpout);
        return;
}
/*-------------------------------------------------------------------------------------*/
/*read input (command) and store it as a string in an array with \0 insted of \n in the end*/
void read_command(char line[]){
	char* word;
	int count=0;
	while(1){
		int c=fgetc(stdin);
		if(c=='\n'){
			line[count++] = '\0';
			break;
		}else{
			line[count++] = (char) c;
		}
	}
	return;
}
/*--------------------------------------------------------------------*/
/*check for append redirection character in the command and separate the word*/
int check_red3(char command[], char* commands[], int* sep_done){
	char* line=strdup(command);
        int i=*sep_done;

        while(1){
                commands[i]=strsep(&line,"@");
                if(commands[i]==NULL) break;
                i++;
        }
	if(*sep_done==i-1){
                return 0;
        }
        *sep_done=i-1;
        return 1;

}
/*--------------------------------------------------------------------*/
/*check for output redirection character in the command and separate the word*/
int check_red2(char command[], char* commands[], int* sep_done){
        char* line=strdup(command);
        int i=*sep_done;

	while(1){
                commands[i]=strsep(&line,"#");
		if(commands[i]==NULL) break;
		i++;
        }
	if(*sep_done==i-1){
                return 0;
        }
	*sep_done=i-1;
        return 1;

}
/*--------------------------------------------------------------------*/
/*check for input redirection character in the command and separate the word*/
int check_red1(char command[], char* commands[], int* sep_done){
	char* line=strdup(command);
        int i=*sep_done;

        while(1){
                commands[i]=strsep(&line,"|");
                if(commands[i]==NULL) break;
                i++;
        }
	if(*sep_done==i-1){
                return 0;
        }
        *sep_done=i-1;
        return 1;

}
/*--------------------------------------------------------------------*/
/*check for pipe character in the command and separate the word*/
int check_pipe(char command[], char* commands[], int* sep_done){
	char* line=strdup(command);
        int i=*sep_done;

        while(1){
                commands[i]=strsep(&line,">");
                if(commands[i]==NULL) break;
                i++;
        }
	if(*sep_done==i-1){
		return 0;
	}
        *sep_done=i-1;
        return 1;

}
/*----------------------------------------------------------------------*/
/*get a command and separate the word in the params array of strings*/
void store_params(char* command, char* params[]){
        char* line=strdup(command);
        int i=0;
        while(1){
                params[i]=strsep(&line," ");
                if(params[i]==NULL) break;
                i++;
        }

        return;
}
/*----------------------------------------------------------------------*/
/*A try to make signal functions*/
void kill_programm(int sig){
        printf("\nKILLED\n");
	return;
}
/*void pause_programm(int sig){
        printf("\nPAUSED\n");
        return;
}
void continue_programm(int sig){
        printf("\nCONTINUED\n");
        return;
}*/

/*---------------------------------------------------------------------*/
/*execution of a simple command (meaning without pipes)*/
void simple_execute(char* params[],int red_flag){
	if(strcmp(params[0],"exit")==0){ /*the exit command terminates the prompt*/
                printf("Process terminated.\n");
		exit(0);
        }else if(strcmp(params[0],"cd")==0){ /*the cd command changes directory*/
                chdir(params[1]);
        }else if(strcmp(params[0],"help")==0){ /*the cd command changes directory*/
                printf("\nHELP\nThe operations of this prompt are the following:\nPipe operator->'>'\nOnly sequence with 1 or 2 pipes supported\nRedirection operators -> '|' for input, '#' for output, '@'for append\nCOMMANDS\n'exit' for finishing the prompt\n'cd' for chenging directory\nall linux commands\n");
		}else{
                /*Forking a child in order to execute if there's linux command*/
		int pid = fork();
                if (pid == -1) {
                        printf("\nFailed in forking process..\n");
                } else if (pid == 0) {
				if(params[1]) check_red(red_flag, params[1]);/*check for redirection*/
				if (execvp(params[0], params) < 0) {
                                	printf("\nCould not execute command...\n\n");
                    		}
				exit(0);
			} else	{
					/*signal for ctrl-c*/
					signal(SIGINT,kill_programm);
					//signal(SIGTSTP,pause_programm);
					//signal(SIGCONT,continue_programm);
					/*waiting for child to terminate*/
                        		waitpid(pid,NULL,0);
            		}
        }
        return;
}
/*----------------------------------------------------------------------*/
/*execution of a one pipe command*/
void piped_execute(char* commands[], int red_flag){
	char* params1[100];
	char* params2[100];
	char* filename;
	store_params(commands[0],params1); /*1st command */
	store_params(commands[1],params2);/*2nd command*/
	if(commands[2]!=NULL)filename=strdup(commands[2]); /*put last command as redirection file for possible future redirection*/
	int fd[2];
	/*make a pipe*/
	if(pipe(fd)==-1){
		printf("Pipe not done...\n");
		return;
	}
	/*fork 1st child proces*/
	int pid1 = fork();
	if(pid1<0){
		printf("\nFailed in forking process..\n");
		return;
	}

	if(pid1==0){
		/*make first command output to the output
		side of the pipe for the next command to listen*/
		//signal(SIGINT, kill_programm);

		dup2(fd[1], STDOUT_FILENO);
		close(fd[0]);
		close(fd[1]);
		/*if there is input redirection*/
		if(red_flag==1) check_red(1,filename);
 		if (execvp(params1[0], params1) < 0) {
                        printf("\nCould not execute command...\n\n");
                }
	}
	/*fork 2st child proces*/
	int pid2 = fork();
	if(pid2<0){
		printf("\nFailed in forking process..\n");
		return;
	}
	if(pid2==0){
		/*make last command input form the input
		side of the pipe to listen the previous command*/
                //signal(SIGINT, kill_programm);

		dup2(fd[0], STDIN_FILENO);
                close(fd[0]);
                close(fd[1]);
		/*if there is output or append redirection*/
		if(red_flag==2||red_flag==3) check_red(red_flag,filename);
        if (execvp(params2[0], params2) < 0) {
            printf("\nCould not execute command...\n\n");
        }
    }
	close(fd[0]);
    	close(fd[1]);
	waitpid(pid1,NULL,0);
	waitpid(pid2,NULL,0);
	return;
}
/*----------------------------------------------------------------------*/
/*execution of a two pipe command*/
void piped_execute_many(char* commands[],int red_flag){
	char* params[3][100];/*array that contains arrays each of them contains the words of every command*/
	int i,k;
	char* filename;
	if(commands[3]!=NULL)filename=strdup(commands[3]); /*put last command as redirection file for possible future redirection*/
	for(i=0; i<3; i++){
		store_params(commands[i],params[i]); /*separate into words*/
	}

	int fd[2][2];

	pipe(fd[0]); // sets up 1st pipe
	pipe(fd[1]); // sets up 2nd pipe
	int pid[3];
	pid[0]=fork();
	if(pid[0]<0){
                printf("\nFailed in forking process..\n");
                return;
        }

	if (pid[0] == 0){
		//signal(SIGINT, kill_programm);

		/*make first command output to the output
		side of 1st the pipe for the second command to listen*/
		dup2(fd[0][1], STDOUT_FILENO);
		close(fd[0][0]);
		close(fd[0][1]);
		close(fd[1][0]);
		close(fd[1][1]);
		/*if there is input redirection*/
		if(red_flag==1) check_red(1,filename);
		if(execvp(params[0][0], params[0])<0){
			printf("\nCould not execute command...\n\n");
		}
	}else{
		pid[1]=fork();
        	if(pid[1]<0){
                	printf("\nFailed in forking process..\n");
                	return;
        	}

		if (pid[1] == 0){

			/*make 2nd command input from the input
			side of the pipe to listen the previous command*/
			dup2(fd[0][0], STDIN_FILENO);
			/*make second command output to the output
			side of the 2nd pipe for the last command to listen*/
			dup2(fd[1][1], STDOUT_FILENO);
			close(fd[0][0]);
			close(fd[0][1]);
			close(fd[1][0]);
			close(fd[1][1]);
			if(execvp(params[1][0], params[1])<0){
				printf("\nCould not execute command...\n\n");
			}
		}else{
			pid[2]=fork();
        		if(pid[2]<0){
                		printf("\nFailed in forking process..\n");
                		return;
        		}
			if (pid[2] == 0){

				/*make 2nd command input from the input
				side of the pipe to listen the previous command*/
				dup2(fd[1][0], STDIN_FILENO);
				close(fd[0][0]);
				close(fd[0][1]);
				close(fd[1][0]);
				close(fd[1][1]);
				/*if there is output or append redirection*/
				if(red_flag==2||red_flag==3) check_red(red_flag,filename);
				if(execvp(params[2][0], params[2])<0){
					printf("\nCould not execute command...\n\n");
				}
			}
		}
	}
	close(fd[0][0]);
	close(fd[0][1]);
	close(fd[1][0]);
	close(fd[1][1]);

	for (i = 0; i < 3; i++){
		waitpid(pid[i],NULL,0);
	}
	return;
}
/*----------------------------------------------------------------------*/
void main(){
	char* params[100];
	char command[1024];
	char* commands[100];
	int sep_done;/*until the separation is*/
	int red3_flag=0;
    	int red2_flag=0;
    	int red1_flag=0;
    	int pipe_flag=0;
	int k=0;
	while(1){
		type_prompt(); /* display prompt on the screen*/
		read_command(command);
		int tmpin=dup(0);/*for future input redirection restore*/
		int tmpout=dup(1);/*for future output redirection restore*/
		sep_done=0;/*separate from start*/
		pipe_flag=check_pipe(command, commands, &sep_done); /*separate pipes*/
		red1_flag=check_red1(commands[sep_done], commands, &sep_done);/*separate |*/
		red2_flag=check_red2(commands[sep_done], commands, &sep_done);/*separate #*/
		red3_flag=check_red3(commands[sep_done], commands, &sep_done);/*separate @*/
		/*for(k=0; k<=sep_done; k++){
			printf("%s\n",commands[k]);
		}*/
		sep_done++;/*increment to work as length*/
		if(sep_done==1){/*for one command simple execution*/
            store_params(commands[0],params);
			simple_execute(params,0);
        }else if(pipe_flag==1){ /*for piped commands*/
			if(sep_done==2){/*if tere are 2 there won't be redirection se 2 pipes execution*/
				piped_execute(commands,0);
			}else if(sep_done==3 && red1_flag==0 && red2_flag==0 && red3_flag==0){/*if there are 3 commands and no redirection 3 pipes execution*/
				piped_execute_many(commands,0);
			}else if(sep_done==3 && (red1_flag==1 || red2_flag==1 || red3_flag==1)){ /*if there are 3 commands and a redirection 2 pipes execution and change red_flag argument properly*/
				if(red1_flag==1){
					piped_execute(commands,1);
				}else if(red2_flag==1){
					piped_execute(commands,2);
				}else{
					piped_execute(commands,3);
				}
            }else if(sep_done==4 && (red1_flag==1 || red2_flag==1 || red3_flag==1)){ /*if there are 4 commands and a redirection 3 pipes execution and change red_flag argument properly*/
				if(red1_flag==1){
                    piped_execute_many(commands,1);
                }else if(red2_flag==1){
                    piped_execute_many(commands,2);
                }else{
                    piped_execute_many(commands,3);
                }

			}else{/*more pipes not supported*/
				printf("This shell doesn't support this command\n");
			}
		}else{/*if no pipes and only redirection simple execution and change red_flag argument properly*/
			if(red1_flag==1){
                change_input(commands[sep_done-1]);
                store_params(commands[0],params);
                simple_execute(params,1);
            }else if(red2_flag==1){
                change_output(commands[sep_done-1]);
				store_params(commands[0],params);
                simple_execute(params,2);
            }else{
            	change_append(commands[sep_done-1]);
				store_params(commands[0],params);
                simple_execute(params,3);
            }
		}
		/*restore redirection for the nxt command*/
		dup2(tmpin,0);
		dup2(tmpout,1);
		close(tmpin);
		close(tmpout);
	}/*end of infinite while loop*/

	return;
}
